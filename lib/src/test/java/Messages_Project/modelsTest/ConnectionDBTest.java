package Messages_Project.modelsTest;

import org.junit.Assert;
import org.junit.Test;

import Messages_Project.models.ConnectionDB;

public class ConnectionDBTest {

	@Test
	public void testConnectionDB() {
		
		ConnectionDB.getConnection();
		
		Assert.assertEquals("Connection successfully!", ConnectionDB.getMessage());
		
	}
}
