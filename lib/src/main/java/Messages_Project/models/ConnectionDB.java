package Messages_Project.models;

import java.sql.DriverManager;

import com.google.protobuf.Message;

import java.sql.Connection;

public class ConnectionDB {
	
	private static Connection con = null;
	private static String url = "jdbc:mysql://localhost:3306/bd_messages";
	private static String user = "root";
	private static String pass = "";
	private static String message = "";
	
	private ConnectionDB() {
		
	}
	
	public static Connection getConnection() {
		
		if(con != null) {
			
			return con;
		}
		
		try {
			
			con = DriverManager.getConnection(url, user, pass);
			message = "Connection successfully!";
			return con;
			
		}catch(Exception e) {
			message = "[ERROR] -> " + e.getMessage();
			return null;
		}
		
		
	}
	
	public static String getMessage() {
		return message;
	}

}
